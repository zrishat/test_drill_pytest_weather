import pytest
from weather_config import weather_csv_path
from weather_config import SUPPORTED_CITIES


def pytest_addoption(parser):
    parser.addoption("--city", action="store", required=True,
                     help="pass either San-Francisco or Hoboken")


@pytest.fixture(scope='module')
def test_data(pytestconfig):
    assert pytestconfig.getoption("city") in SUPPORTED_CITIES  # more DDD is never bad
    return deserialize_monthly_average_high(), get_current_month(), pytestconfig.getoption("city")


def deserialize_monthly_average_high():
    """
    Read csv and return dictionary with cities' monthly average high temp
    """
    import csv
    from collections import defaultdict
    with open(weather_csv_path, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        weather = defaultdict(dict)
        for row in reader:
            if row:
                weather[row[0].strip()].update({row[1].strip(): row[2].strip()})
    return weather


def get_current_month():
    from datetime import datetime
    import calendar
    month = datetime.now().month
    month_str = calendar.month_name[month]
    return month_str


if __name__ == '__main__':
    # deserialize_monthly_average_high()
    get_current_month()
