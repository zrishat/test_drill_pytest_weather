from weather_config import ALLOWED_TEMPERATURE_RANGE


def get_current_weather_for(city):
    """
    Returns  fahrenheit representation of current air temperature for the specified city
    :param city:
    :return: int
    """
    from weather import Weather, Unit
    weather = Weather(unit=Unit.FAHRENHEIT)
    return weather.lookup_by_location(city).condition.temp


def test_near_highest(test_data):
    """
    Given: City name, its current air temperature and current month
    When: Current temperature is within 2F of month's average high
    Then: Test pass
    Else: Test should fail
    """
    weather, month, city = test_data
    min_boundary = int(weather[city][month]) - ALLOWED_TEMPERATURE_RANGE
    max_boundary = int(weather[city][month]) + ALLOWED_TEMPERATURE_RANGE
    assert min_boundary <= int(get_current_weather_for(city)) <= max_boundary
